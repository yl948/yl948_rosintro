#!/bin/bash
# Set the pen color for drawing 'y' to red
rosservice call /turtle1/set_pen 255 0 0 5 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[1.0, -1.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[1.0, 1.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-2.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'

#set initial position of second turtle
rosservice call /spawn 9 3 0.2 ""
# Set the pen color for drawing 'a' to yello
rosservice call /turtle2/set_pen 255 255 0 5 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- '[-1.0, 0.0, 0.0]' '[0.0, 0.0, -0.5]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- '[-2.0, 1.0, 0.0]' '[0.0, 0.0, -3.5]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- '[-1.0, 1.3, 0.0]' '[0.0, 0.0, 1.0]'
