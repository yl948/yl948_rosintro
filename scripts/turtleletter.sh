#!/usr/bin/bash
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[5.0, 0.0, 0.0]' '[0.0, 0.0, -4.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[2.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-3.0, 3.0, 0.0]' '[0.0, 0.0, 0.0]'
