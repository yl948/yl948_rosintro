#!/usr/bin/env python

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, SRI International
# All rights reserved.
#
# part of the code are borrowed from the example

from __future__ import print_function
from six.moves import input
import time

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

## END_SUB_TUTORIAL


def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True

class MidtermInteface(object):
    """
    Define a class to interact with MoveIt! for motion planning.
    Constructor initializes the robot, its scene, and the move group.
    """
    def __init__(self):
        super(MidtermInteface, self).__init__()
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        self.robot = moveit_commander.RobotCommander()
        ## Instantiate a `PlanningSceneInterface`_ object that provides a remote interface for 
        ## getting, setting, and updating the robot's internal understanding of the urrounding world
        self.scene = moveit_commander.PlanningSceneInterface()
        # Define the move group.
        group_name = "manipulator"
        self.move_group = moveit_commander.MoveGroupCommander(group_name)

    def go_to_joint_state(self, state):
        '''
        Planning to a Joint Goal. modified from the provided example, so
         we can change the joint state when we call the function
        '''
        move_group = self.move_group
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = state[0]
        joint_goal[1] = state[1]
        joint_goal[2] = state[2]
        joint_goal[3] = state[3]
        joint_goal[4] = state[4]
        joint_goal[5] = state[5]  # 1/n of a turn

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)
        move_group.go(joint_goal, wait=True)

        move_group.stop() # Calling ``stop()`` ensures that there is no residual movement
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)
    
    def go_to_pose(self, move_group, x, y, z):
        ''' 
        Copy class variables to local variables to make the web tutorials more clear.
        Modified from provided example, so we can directly assign different poses 
        when calling the function. x,y,z are the value in different axis, w is fixed to 1
        '''

        # Define the desired pose
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 1.0
        pose_goal.position.x = x
        pose_goal.position.y = y
        pose_goal.position.z = z

        # Set the pose target and execute the motion
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()

        current_pose = self.move_group.get_current_pose().pose
        # print("current_pose is ", current_pose)
        return all_close(pose_goal, current_pose, 0.01)


    def draw_initials_Y(self, scale=1):
        """
        Command the robot to draw the letter 'Y' using waypoints and Cartesian path planning.
        It sets waypoints for the end effector to follow to draw letter y.
        After setting the waypoints, it computes the path and executes the movement.

        Args:
        - scale (float): Scaling factor for the size of the letter.
        """
        
        # Initial preparation and information display.
        print("starting to draw Y in 3 seconds...")
        time.sleep(3)
        waypoints = []

        # Capture the robot's current pose and joint values.
        wpose = self.move_group.get_current_pose().pose
        current_joints = self.move_group.get_current_joint_values()
        print("init_pose_y", wpose)
        print("init_joints_y:",current_joints)

        # Define waypoints for the robot's end effector to draw the letter 'y'.
        wpose.position.z -= scale * 0.1
        waypoints.append(copy.deepcopy(wpose))
        wpose.position.x -= 0.1
        waypoints.append(copy.deepcopy(wpose))
        wpose.position.z += 0.1
        waypoints.append(copy.deepcopy(wpose))
        wpose.position.z -= 0.2
        waypoints.append(copy.deepcopy(wpose))
        wpose.position.x += 0.05
        waypoints.append(copy.deepcopy(wpose))

         # Execute the movement to draw the 'Y' using the defined waypoints.
        (plan, fraction) = self.move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
        self.move_group.execute(plan, wait=True)

        # Display the end pose and joint angles after drawing.
        end_pose_y = self.move_group.get_current_pose().pose
        current_joints = self.move_group.get_current_joint_values()
        print("end_pose_y:", end_pose_y)
        print("end_joints_y:",current_joints)

    def re_locate(self):
        """
        Adjusts the robot's end effector to a predefined location. 
        This function is typically used to reposition the end effector after 
        drawing one letter, preparing it to start drawing the next letter.
        """
        print("Move EE up and get ready to draw next letter...")
        time.sleep(2) # A short pause to ensure the user has time to observe the relocation.
        move_group = self.move_group
        
        # Set the n good starting position for the end effector to draw the next letter.
        self.go_to_pose(move_group, 0.326, 0.480, 0.736)


    def draw_initials_H(self):
        """
        Command the robot to draw the letter 'H' using waypoints and Cartesian path planning.
        It sets waypoints for the end effector to follow to draw letter H.
        After setting the waypoints, it computes the path and executes the movement.
        """
        print("starting to draw H in 3 seconds...")
        time.sleep(3)
        waypoints = []

        wpose = self.move_group.get_current_pose().pose
        current_joints = self.move_group.get_current_joint_values()
        print("init_pose_H", wpose)
        print("init_joints_H:",current_joints)

        wpose.position.z -= 0.2 # go down for first stoke
        waypoints.append(copy.deepcopy(wpose))
        wpose.position.z += 0.1 # go up to middle of first stoke
        waypoints.append(copy.deepcopy(wpose))
        wpose.position.x -= 0.1 # drasw small dash between two vertical line
        waypoints.append(copy.deepcopy(wpose))
        wpose.position.z += 0.1 # go up
        waypoints.append(copy.deepcopy(wpose))
        wpose.position.z -= 0.2 # finish drawing the last stoke
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = self.move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
        self.move_group.execute(plan, wait=True)

        # print current pose and joint angles
        end_pose_H = self.move_group.get_current_pose().pose
        current_joints = self.move_group.get_current_joint_values()
        print("end_pose_H:", end_pose_H)
        print("end_joints_H:",current_joints)


    def draw_initials_L(self):
        """
        Command the robot to draw the letter 'L' using waypoints and Cartesian path planning.
        It sets waypoints for the end effector to follow to draw letter L.
        After setting the waypoints, it computes the path and executes the movement.
        """
        print("starting to draw L in 3 seconds...")
        time.sleep(3)
        waypoints = []
        wpose = self.move_group.get_current_pose().pose
        current_joints = self.move_group.get_current_joint_values()
        print("init_pose_L", wpose)
        print("init_joints_L:",current_joints)

        wpose.position.z -= 0.2 # go down for the first stroke
        waypoints.append(copy.deepcopy(wpose))
        wpose.position.x -= 0.1 # go left for the second stroke
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = self.move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
        self.move_group.execute(plan, wait=True)

        # print current pose and joint angles
        end_pose_L = self.move_group.get_current_pose().pose
        current_joints = self.move_group.get_current_joint_values()
        print("end_pose_L:", end_pose_L)
        print("end_joints_L:",current_joints)



def main():
    """
    The main function initializes the robot interface and controls it to draw the initials 'YHL'.
    """
    try:
        tutorial = MidtermInteface() # Create an instance of the robot interface.
        tutorial.go_to_joint_state([0, 0, 0, 0, 0, 0]) # Initial setup: Move robot to default pose.
        time.sleep(3) #wait for 3 second
        tutorial.go_to_joint_state([tau/8, -tau /15,-tau/7, -tau/15, tau/4, -tau/6 ])  # Set the starting pose for drawing.

        # Command the robot to draw each letter in sequence.
        tutorial.draw_initials_Y() #draw letter Y
        tutorial.re_locate() #relocate the EE to initial position
        tutorial.draw_initials_H() #draw H
        tutorial.re_locate() #relocate the EE to initial position
        tutorial.draw_initials_L() #draw L

        print("============ Python draw initial complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()
